﻿using CS481_HW4.Formats;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
    public partial class RecipiePage : ContentPage
    {
        public RecipiePage(int i, string s)
        {
            InitializeComponent();

            PopulateRecipie(i, s);
        }
        private void PopulateRecipie(int id, string rname)
        {
            //Fills the recipie page with the data of the item selected
            string[] rectext = readFiles(id);
            BindingContext = new RecPg()
            {
                RecipieName = rname,
                RecipieIcon = String.Format("r{0}.png", id),
                RecipieDirections = rectext[0],
                RecipieIngredients = rectext[1]
            };

        }
        void HandleWebSearch(object sender, System.EventArgs e)
        {
            //Fetch the recipie name from the page
            List<string> keys = RecName.Text.Split(' ').ToList<string>();
            string search = "";
            //Join the keywords to match the search engine url format
            search = string.Join("+", keys);
            string eurl = String.Format("http://www.google.com/search?q={0}", search);
            //Opens the URL in the device's default web browser
            Device.OpenUri(new Uri(eurl));
        }

        string[] readFiles(int id)
        {
            #if __IOS__
            var resourcePrefix = "CS481_HW4.iOS.";
            #endif
            #if __ANDROID__
            var resourcePrefix = "CS481_HW4.Droid.";
            #endif
            // note that the prefix includes the trailing period '.' that is required
            //ensure all text files are of type "Embedded Resource"
            //This method allows for the reuse of the recipie page across all recipies
            var assembly = typeof(RecipiePage).GetTypeInfo().Assembly;
            Stream stream1 = assembly.GetManifestResourceStream(resourcePrefix + "Assets.RecDir." + String.Format("d{0}.txt", id));
            Stream stream2 = assembly.GetManifestResourceStream(resourcePrefix + "Assets.RecIng." + String.Format("i{0}.txt", id));
            string[] text = new string[2];
            using (var reader = new System.IO.StreamReader(stream1))
            {
                //Pulls the directions from the file
                text[0] = reader.ReadToEnd();
            }
            using (var reader = new System.IO.StreamReader(stream2))
            {
                //Pulls the ingredients from the file
                text[1] = reader.ReadToEnd();
            }
            return text;
        }
    }
}