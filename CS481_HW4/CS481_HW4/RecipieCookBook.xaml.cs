﻿using CS481_HW4.Formats;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
    
	public partial class RecipieCookBook : ContentPage
	{

        ObservableCollection<RecipieFormat> book = new ObservableCollection<RecipieFormat>();
        public RecipieCookBook ()
		{
			InitializeComponent ();

            PopulateBook();
		}
        private void PopulateBook()
        {
            //Populate the list with entries
            book.Add(new RecipieFormat
            {
                RecipieName ="Devil's Food Cake",
                RecipieDesc ="A sinful decadence of Chocolate",
                IconPreview ="r1.png",
                RecipieID =1
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "White Chocolate Cheesecake",
                RecipieDesc = "Heavy yet light, a summertime delight",
                IconPreview = "r2.png",
                RecipieID = 2
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Chocolate Chip Cookies",
                RecipieDesc = "The Baker's Staple",
                IconPreview = "r3.png",
                RecipieID = 3
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Pecan Pie",
                RecipieDesc = "An American Southern Classic",
                IconPreview = "r4.png",
                RecipieID = 4
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Fudge Brownies",
                RecipieDesc = "Pure chocolate, turned up to 11",
                IconPreview = "r5.png",
                RecipieID = 5
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Chocolate Silk Pie",
                RecipieDesc = "Smooth and sweet as it's namesake",
                IconPreview = "r6.png",
                RecipieID = 6
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Chocolate Souffle",
                RecipieDesc = "The pinnacle of skill, with a dust of sweetness",
                IconPreview = "r7.png",
                RecipieID = 7
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Baklava",
                RecipieDesc = "A European Tradition, right at your door",
                IconPreview = "r8.png",
                RecipieID = 8
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Croquembouche",
                RecipieDesc = "The leaning tower of sugar",
                IconPreview = "r9.png",
                RecipieID = 9
            });
            book.Add(new RecipieFormat
            {
                RecipieName = "Molten Lava Cake",
                RecipieDesc = "The secret surprise inside",
                IconPreview = "r10.png",
                RecipieID = 10
            });
            CookBookList.ItemsSource = book;
        }
        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            //Empties the list (in order to prevent data mis-match)
            book.Clear();
            //Then refills the list
            PopulateBook();
            CookBookList.IsRefreshing = false;
        }
        void Handle_ItemTap(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            //When user selects a recipie, call it's recipie page, Old
            var selectedRecipie = (ListView)sender;
            RecipieFormat rp = (RecipieFormat)selectedRecipie.SelectedItem;
            Navigation.PushAsync(new RecipiePage(rp.RecipieID, rp.RecipieName));
        }
        void Handle_OpenRecipieButton(object sender, System.EventArgs e)
        {
            //When user selects a recipie, call it's recipie page, new
            var selectedRecipie = (MenuItem)sender;
            RecipieFormat rp = (RecipieFormat)selectedRecipie.CommandParameter;
            Navigation.PushAsync(new RecipiePage(rp.RecipieID, rp.RecipieName));
        }
        void Handle_Delete(object sender, System.EventArgs e)
        {
            //Delete item from list
            var selectedRecipie = (MenuItem)sender;
            RecipieFormat rp = (RecipieFormat)selectedRecipie.CommandParameter;
            book.Remove(rp);
        }
    }
}