﻿1/2 cup butter
3 (1 ounce) squares unsweetened chocolate
2 cups white sugar
2 eggs 
1 cup water
2 1/4 cups all-purpose flour
1 1/2 teaspoons baking soda
1/4 teaspoon salt
1/4 cup milk
1 teaspoon distilled white vinegar