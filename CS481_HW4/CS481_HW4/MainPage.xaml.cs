﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW4
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        private void ListViewOpen(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new RecipieCookBook());
        }

    }
}
