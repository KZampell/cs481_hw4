﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace CS481_HW4.Formats
{
	public class RecipieFormat 
	{
		public string RecipieName
        {
            get;
            set;
        }
        public string RecipieDesc
        {
            get;
            set;
        }
        public int RecipieID
        {
            get;
            set;
        }
        public ImageSource IconPreview
        {
            get;
            set;
        }
	}
}