﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace CS481_HW4.Formats
{
	public class RecPg
	{
        public string RecipieName
        {
            get;
            set;
        }
        public string RecipieIngredients
        {
            get;
            set;
        }
        public string RecipieDirections
        {
            get;
            set;
        }
        public ImageSource RecipieIcon
        {
            get;
            set;
        }
    }
}